<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Survey extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'surveys';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $hidden = ['updated_at', 'deleted_at'];

    /**
     * @return HasMany
     */
    public function questions(): HasMany
    {
        return $this->hasMany('App\Models\SurveyQuestion', 'survey_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function respondents(): HasMany
    {
        return $this->hasMany('App\Models\SurveyRespondent', 'survey_id', 'id');
    }

}
