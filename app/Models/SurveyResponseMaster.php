<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class SurveyResponseMaster extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'survey_responses_master';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $hidden = ['updated_at', 'deleted_at'];

    /**
     * Get the Survey respondent that owns the Survey Response.
     */
    public function respondent(): BelongsTo
    {
        return $this->belongsTo('App\Models\SurveyRespondent', 'respondent_id', 'id');
    }

    /**
     * Get the Survey that owns the Survey Question.
     */
    public function survey(): BelongsTo
    {
        return $this->belongsTo('App\Models\Survey', 'survey_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function details(): HasMany
    {
        return $this->hasMany('App\Models\SurveyResponseDetails', 'master_id', 'id');
    }

}
