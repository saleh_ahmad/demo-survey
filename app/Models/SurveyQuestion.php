<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class SurveyQuestion extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'survey_questions';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $hidden = ['updated_at', 'deleted_at'];

    /**
     * Get the Survey that owns the Survey Question.
     */
    public function survey(): BelongsTo
    {
        return $this->belongsTo('App\Models\Survey', 'survey_id', 'id');
    }

    /**
     * Get the Question Type that owns the Survey Question.
     */
    public function questionType(): BelongsTo
    {
        return $this->belongsTo('App\Models\SurveyQuestionType', 'question_type_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function choices(): HasMany
    {
        return $this->hasMany('App\Models\SurveyQuestionChoice', 'question_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function response(): HasMany
    {
        return $this->hasMany('App\Models\SurveyResponse', 'question_id', 'id');
    }

}
