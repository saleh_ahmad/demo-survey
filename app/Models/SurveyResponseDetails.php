<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class SurveyResponseDetails extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'survey_responses_details';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $hidden = ['updated_at', 'deleted_at'];

    /**
     * Get the Survey Question that owns the Survey Response.
     */
    public function question(): BelongsTo
    {
        return $this->belongsTo('App\Models\SurveyQuestion', 'question_id', 'id');
    }

    /**
     * Get the Survey Question Choice that owns the Survey Response.
     */
    public function choice(): BelongsTo
    {
        return $this->belongsTo('App\Models\SurveyQuestionChoice', 'choice_id', 'id');
    }

    /**
     * Get the Survey Response Master that owns the Survey Response Details.
     */
    public function master(): BelongsTo
    {
        return $this->belongsTo('App\Models\SurveyResponseMaster', 'master_id', 'id');
    }

}
