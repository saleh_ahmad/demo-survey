<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class SurveyQuestionChoice extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'survey_question_choices';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $hidden = ['updated_at', 'deleted_at'];

    /**
     * Get the Survey Question that owns the Survey Question Choices.
     */
    public function surveyQuestion(): BelongsTo
    {
        return $this->belongsTo('App\Models\SurveyQuestion', 'question_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function response(): HasMany
    {
        return $this->hasMany('App\Models\SurveyResponse', 'choice_id', 'id');
    }

}
