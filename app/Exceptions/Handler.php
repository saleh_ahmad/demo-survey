<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Throwable;
use ParseError;
use TypeError;
use BadMethodCallException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            $this->renderable(function (ParseError $ex, $request) {
                abort(405, 'ParseError: '. $ex->getMessage());
            });
            $this->renderable(function (TypeError $ex, $request) {
                abort(405, 'TypeError: Please provide correct return type in Method.');
            });

            $this->renderable(function (BadMethodCallException $e, $request) {
                abort(405, 'BadMethodCallException: Method does not exist in Controller.');
            });

            $this->renderable(function (MethodNotAllowedHttpException $e, $request) {
                abort(405, 'MethodNotAllowedHttpException: ' . $e->getMessage() . '.');
            });
        });
    }
}
