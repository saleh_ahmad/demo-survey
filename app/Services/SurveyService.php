<?php

namespace App\Services;
use App\Models\SurveyQuestion;
use App\Models\SurveyRespondent;
use App\Models\SurveyResponseDetails;
use App\Models\SurveyResponseMaster;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class SurveyService
{
    public function getSurveyQuestionsWithOptions($id)
    {
        $surveyQuestions = SurveyQuestion::select('id', 'text', 'question_type_id', 'survey_id')
            ->with(['survey:id,name'])
            ->with(['questionType:id,name'])
            ->with(['choices:id,text,question_id'])
            ->where('survey_id', $id)
            ->orderBy('order_by')
            ->get();

        if (!count($surveyQuestions)) {
            throw new ModelNotFoundException('Survey Not Found.');
        }

        return $surveyQuestions;
    }

    public function getSurveyResponses($id)
    {
        $surveyResponses = SurveyResponseMaster::select('id', 'survey_id', 'respondent_id')
            ->with(['details:id,master_id,question_id,text,choice_id', 'details.question:id,text,question_type_id', 'details.choice:id,text,question_id'])
            ->with(['survey:id,name'])
            ->with(['respondent:id,name,phone,email'])
            ->where('survey_id', $id)
            ->orderByDesc('created_at')
            ->get();

        if (!count($surveyResponses)) {
            throw new ModelNotFoundException('Survey Not Found.');
        }

        return $surveyResponses;
    }

    public function validateInsertData($request)
    {
        $rules = [
            'q_1' => 'required|integer',
            'q_2' => 'required_if:q_1,3|array',
            'q_3' => 'required_if:q_1,1,3',
            'q_4' => 'required_if:q_1,1,3',
            'q_5' => 'required|integer',
            'q_6' => 'required_if:q_5,8',
            'q_7' => 'required_if:q_5,9|integer',
        ];

        $messages = [
            'q_1.required'    => 'Please answer to the Question No. 1.',
            'q_1.integer'     => 'Please answer to the Question No. 1 properly.',
            'q_2.required_if' => 'Please answer to the Question No. 2.',
            'q_2.array'       => 'Please answer to the Question No. 2 properly.',
            'q_3.required_if' => 'Please answer to the Question No. 3.',
            'q_4.required_if' => 'Please answer to the Question No. 4.',
            'q_5.required'    => 'Please answer to the Question No. 5.',
            'q_5.integer'     => 'Please answer to the Question No. 5 properly.',
            'q_6.required_if' => 'Please answer to the Question No. 6.',
            'q_7.required'    => 'Please answer to the Question No. 7.',
            'q_7.integer'     => 'Please answer to the Question No. 7 properly.',
        ];

        $request
            ->validate($rules, $messages);
    }

    public function insert($request)
    {
        $q1 = $request->input('q_1');
        $q2 = $request->input('q_2');
        $q3 = $request->input('q_3');
        $q4 = $request->input('q_4');
        $q5 = $request->input('q_5');
        $q6 = $request->input('q_6');
        $q7 = $request->input('q_7');
        $q8 = $request->input('q_8');

        $name  = $request->input('name');
        $phone = $request->input('phone');
        $email = $request->input('email');

        # Save Respondent's Information
        if ($name || $phone || $email) {
            $surveyRespondent = new SurveyRespondent;

            $surveyRespondent->name  = $name ?? null;
            $surveyRespondent->phone = $phone ?? null;
            $surveyRespondent->email = $email ?? null;

            $surveyRespondent->save();
        }

        # Save Respondent's Response
        $answers = [];
        $respondentId = ($name || $phone || $email) ? $surveyRespondent->id : 0;

        # Save Survey Response Master
        $surveyResponseMaster = new SurveyResponseMaster;

        $surveyResponseMaster->respondent_id = $respondentId;
        $surveyResponseMaster->survey_id     = 1;

        $surveyResponseMaster->save();

        if ($q1) {
            $answers[] = [
                'master_id'     => $surveyResponseMaster->id,
                'question_id'   => 1,
                'text'          => '',
                'choice_id'     => $q1,
                'updated_at'    => now(),
                'created_at'    => now()
            ];
        }

        if ($q2) {
            $q2Answer = implode(',', $q2);

            foreach ($q2 as $choice) {
                $answers[] = [
                    'master_id'     => $surveyResponseMaster->id,
                    'question_id'   => 2,
                    'text'          => '',
                    'choice_id'     => (int)$choice,
                    'updated_at'    => now(),
                    'created_at'    => now()
                ];
            }
        }

        if ($q3) {
            $answers[] = [
                'master_id'     => $surveyResponseMaster->id,
                'question_id'   => 3,
                'text'          => $q3,
                'choice_id'     => 0,
                'updated_at'    => now(),
                'created_at'    => now()
            ];
        }

        if ($q4) {
            $answers[] = [
                'master_id'     => $surveyResponseMaster->id,
                'question_id'   => 4,
                'text'          => $q4,
                'choice_id'     => 0,
                'updated_at'    => now(),
                'created_at'    => now()
            ];
        }

        if ($q5) {
            $answers[] = [
                'master_id'     => $surveyResponseMaster->id,
                'question_id'   => 5,
                'text'          => '',
                'choice_id'     => $q5,
                'updated_at'    => now(),
                'created_at'    => now()
            ];
        }

        if ($q6) {
            $answers[] = [
                'master_id'     => $surveyResponseMaster->id,
                'question_id'   => 6,
                'text'          => $q6,
                'choice_id'     => 0,
                'updated_at'    => now(),
                'created_at'    => now()
            ];
        }

        if ($q7) {
            $answers[] = [
                'master_id'     => $surveyResponseMaster->id,
                'question_id'   => 7,
                'text'          => '',
                'choice_id'     => $q7,
                'updated_at'    => now(),
                'created_at'    => now()
            ];
        }

        if ($q8) {
            $answers[] = [
                'master_id'     => $surveyResponseMaster->id,
                'question_id'   => 8,
                'text'          => $q8,
                'choice_id'     => 0,
                'updated_at'    => now(),
                'created_at'    => now()
            ];
        }

        if (sizeof($answers)) {
            SurveyResponseDetails::insert($answers);
        }

        return true;
    }

}
