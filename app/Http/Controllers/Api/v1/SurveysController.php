<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\SurveyQuestion;
use App\Models\SurveyResponse;
use App\Services\SurveyService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use InvalidArgumentException;
use Error;
use ErrorException;
use Exception;

class SurveysController extends Controller
{
    /**
     * @param $id
     * @param SurveyService $surveyservice
     * @return JsonResponse
     */
    public function index($id, SurveyService $surveyservice): JsonResponse
    {
        try {
            $response = [
                'error'   => false,
                'success' => false
            ];

            $response['surveyAnswers'] = $surveyservice->getSurveyResponses($id);
        } catch (ModelNotFoundException $ex) {
            $response['error'] = true;
            $response['errors']['notFound'] = ['Survey Not Found.'];

            return response()
                ->json($response, 400, [], JSON_PRETTY_PRINT);
        } catch (InvalidArgumentException | Error | ErrorException | QueryException $ex) {
            $response['error'] = true;
            $response['errors']['exception'] = [$ex->getMessage()];

            return response()
                ->json($response, 400, [], JSON_PRETTY_PRINT);
        }

        $response['success'] = true;
        return response()
            ->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function create($id, SurveyService $surveyservice): JsonResponse
    {
        try {
            $response = [
                'error'   => false,
                'success' => false
            ];

            $response['surveyQuestions'] = $surveyservice->getSurveyQuestionsWithOptions($id);
        } catch (ModelNotFoundException | Error | ErrorException | QueryException $ex) {
            $response['error'] = true;
            $response['errors']['exception'] = [$ex->getMessage()];

            return response()
                ->json($response, 400, [], JSON_PRETTY_PRINT);
        }

        $response['success'] = true;
        return response()
            ->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    /**
     * @param Request $request
     * @param SurveyService $surveyservice
     * @return JsonResponse
     */
    public function store(Request $request, SurveyService $surveyservice): JsonResponse
    {
        try {
            $response = [
                'error'   => false,
                'success' => false
            ];

            $surveyservice->validateInsertData($request);
            $surveyservice->insert($request);
        } catch (ValidationException $ex) {
            $response['error']  = true;
            $response['errors'] = $ex->errors();

            return response()
                ->json($response, 400, [], JSON_PRETTY_PRINT);
        }  catch (QueryException $ex) {
            $response['error'] = true;
            $response['errors']['query'] = [$ex->getMessage()];

            return response()
                ->json($response, 400, [], JSON_PRETTY_PRINT);
        } catch (InvalidArgumentException | Error | ErrorException | Exception $ex) {
            $response['error'] = true;
            $response['errors']['exception'] = [$ex->getMessage()];

            return response()
                ->json($response, 400, [], JSON_PRETTY_PRINT);
        }

        $response['success'] = true;
        return response()
            ->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
