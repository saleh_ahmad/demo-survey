<?php

namespace App\Http\Controllers;

use App\Models\SurveyQuestion;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\View as ViewClass;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Error;
use ErrorException;


class HomeController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        try {
            if (!ViewClass::exists('home')) {
                throw new InvalidArgumentException('View file "home" not found.');
            }
        }  catch (InvalidArgumentException | Error | ErrorException $ex) {
            abort(405, 'Exception: '. $ex->getMessage());
        }

        return view('home');
    }

    /**
     * @return View
     */
    public function allSurveyData(): View
    {
        try {
            if (!ViewClass::exists('allData')) {
                throw new InvalidArgumentException('View file "allData" not found.');
            }
        }  catch (InvalidArgumentException | Error | ErrorException $ex) {
            abort(405, 'Exception: '. $ex->getMessage());
        }

        return view('allData');
    }

}
