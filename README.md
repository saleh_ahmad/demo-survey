### Instruction to run the application

- Clone this project on www folder
- Install Composer by using `composer install`
- Update your configuration from .env file
- Create a Database in Postgres
- Run migration using `php artisan migrate`
- Start Laragon
- Run project using the url "http://127.0.0.1/ifarmer-survey"
