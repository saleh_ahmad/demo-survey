<?php

use App\Http\Controllers\Api\v1\SurveysController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('v1/surveys/{id}/answers', [SurveysController::class, 'store']);
Route::get('v1/surveys/{id}', [SurveysController::class, 'create']);
Route::get('v1/surveys/{id}/answers', [SurveysController::class, 'index']);
