<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <title>503 Service Unavailable</title>
    <link rel="stylesheet" href="{{ asset('resources/errors/500/style.css') }}">

    {{-- Favicon --}}
    <x-favicon />
</head>
<body>
<!-- partial:index.partial.html -->
<div class="supper-man">
    <img src="{{ asset('resources/errors/500/superman_PNG9.png') }}" alt="Superman">
</div>
<div class="title">503!</div>
<P>Sorry, it's me, not you. :(</p>
<p>Let me try again!</p>
<a href="{{url('')}}"><button>Home</button></a>
<!-- partial -->
<script  src="{{ asset('resources/errors/500/script.js') }}"></script>

</body>
</html>
