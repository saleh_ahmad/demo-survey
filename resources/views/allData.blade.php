{{-- Extends layout --}}
@extends('layouts.common')

{{-- Content --}}
@section('content')
    <section class="container">
        <div class="row my-5">
            <div class="col" id="all_surveys"></div>
        </div>
    </section>
@endsection

{{-- Title Section --}}
@section('title', 'All Data')

{{-- Meta Section --}}
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('resources/js/surveyList.js') }}"></script>
@endsection
