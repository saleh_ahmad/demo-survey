{{-- Extends layout --}}
@extends('layouts.common')

{{-- Content --}}
@section('content')
<section class="container">
    <div class="row my-5">
        <div class="col">
            <form id="survey-form"
                  method="post"
                  action="{{ url('api/v1/surveys/1/answers') }}">
                <div class="d-grid gap-3" id="survey-block"></div>
                <div id="survey-validation-alert" class="mt-2"></div>
                <div class="float-end mt-3 submit-btn"></div>
            </form>
        </div>
    </div>
</section>
@endsection

{{-- Title Section --}}
@section('title', 'Home')

{{-- Meta Section --}}
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('resources/js/survey.js?1') }}"></script>
@endsection
