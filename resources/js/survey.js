const validationAlert = $('#survey-validation-alert');

$(document).ready(function() {
    let surveyHtml = '';

    $.ajax({
        url: HOST_URL +'/api/v1/surveys/1',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        type: 'GET',
        dataType: 'json',
        error: function(response) {
            const errordata = JSON.parse(response.responseText).errors;

            if (errordata.hasOwnProperty('exception')) {
                validationAlert.html('<div class="alert alert-danger">'+ errordata.exception[0] + '</div>');
            }
        },
        success: function(response) {
            surveyHtml += `<h1 class="text-center" id="surveyName">${response.surveyQuestions[0].survey.name}</h1>`;

            let i, j, k;
            for (i = 0; i < response.surveyQuestions.length; ++i) {
                surveyHtml += `<div class="p-2 mt-3 bg-light border survey_div" id="div_${response.surveyQuestions[i].id}">
                            ${i + 1} ${response.surveyQuestions[i].text}
                            <div class="pt-2">`;

                if (parseInt(response.surveyQuestions[i].question_type_id) === 1) {
                    surveyHtml += `<textarea class="form-control" name="q_${i + 1}" id="q_${i + 1}"></textarea>`;
                } else if (parseInt(response.surveyQuestions[i].question_type_id) === 2) {
                    for (j = 0; j <  response.surveyQuestions[i].choices.length; ++j) {
                        surveyHtml += `<div class="form-check">
                        <input class="form-check-input" type="radio" name="q_${i + 1}" value="${response.surveyQuestions[i].choices[j].id}"
                    id="choice_${response.surveyQuestions[i].choices[j].id}">
                        <label class="form-check-label" for="choice_${response.surveyQuestions[i].choices[j].id}">
                        ${response.surveyQuestions[i].choices[j].text}
                        </label>
                        </div>`;
                    }
                } else if (parseInt(response.surveyQuestions[i].question_type_id) === 3) {
                    for (k = 0; k <  response.surveyQuestions[i].choices.length; ++k) {
                        surveyHtml += `<div class="form-check">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   name="q_${i + 1}[]"
                                                   value="${response.surveyQuestions[i].choices[k].id}"
                                                   id="choice_${response.surveyQuestions[i].choices[k].id}">
                                            <label class="form-check-label" for="choice_${response.surveyQuestions[i].choices[k].id}">
                                                ${response.surveyQuestions[i].choices[k].text}
                                            </label>
                                        </div>`;
                    }
                }

                surveyHtml +=`</div></div>`;
            }

            surveyHtml += `<div class="form-group">
                        <label for="name" class="form-label">Name</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="phone" class="form-label">Phone</label>
                        <input type="text" class="form-control" id="phone" name="phone">
                    </div>
                    <div class="form-group">
                        <label for="email" class="form-label">Email</label>
                        <input type="text" class="form-control" id="email" name="email">
                    </div>`;

            $('#survey-block').html(surveyHtml);
            $('.submit-btn').html(`<button class="btn btn-success" type="submit">Submit</button>`);
        }
    });
});

$("#survey-block").on('click', 'input[type=radio]', function() {
    const questionId = parseInt($(this).attr('name').split('_')[1]),
        choiceId = parseInt($(this).val());

    if (questionId === 1 && choiceId === 1) {
        $(this).parent().parent().parent().parent().find('div#div_2').hide();
        $(this).parent().parent().parent().parent().find('div#div_3').show();
        $(this).parent().parent().parent().parent().find('div#div_4').show();
    } else if (questionId === 1 && choiceId === 2) {
        $(this).parent().parent().parent().parent().find('div#div_2').hide();
        $(this).parent().parent().parent().parent().find('div#div_3').hide();
        $(this).parent().parent().parent().parent().find('div#div_4').hide();
    } else if (questionId === 1 && choiceId === 3) {
        $(this).parent().parent().parent().parent().find('div#div_2').show();
        $(this).parent().parent().parent().parent().find('div#div_3').show();
        $(this).parent().parent().parent().parent().find('div#div_4').show();
    } else if (questionId === 5 && choiceId === 7) {
        $(this).parent().parent().parent().parent().find('div#div_6').hide();
        $(this).parent().parent().parent().parent().find('div#div_7').hide();
    }  else if (questionId === 5 && choiceId === 9) {
        $(this).parent().parent().parent().parent().find('div#div_6').hide();
        $(this).parent().parent().parent().parent().find('div#div_7').show();
    } else if (questionId === 5 && choiceId === 8) {
        $(this).parent().parent().parent().parent().find('div#div_6').show();
        $(this).parent().parent().parent().parent().find('div#div_7').show();
    }
});

$('#survey-form').submit(function(e) {
    e.preventDefault();

    const form_data = new FormData(this);

    $.ajax({
        url: HOST_URL +'/api/v1/surveys/1/answers',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        type: 'POST',
        dataType: 'json',
        data: form_data,
        processData: false,
        contentType: false,
        error: function(response) {
            const errordata = JSON.parse(response.responseText).errors;

            if (errordata.hasOwnProperty('q_1')) {
                validationAlert.html('<div class="alert alert-danger">'+ errordata.q_1[0] + '</div>');
            } else if (errordata.hasOwnProperty('q_2')) {
                validationAlert.html('<div class="alert alert-danger">'+ errordata.q_2[0] + '</div>');
            }  else if (errordata.hasOwnProperty('q_3')) {
                validationAlert.html('<div class="alert alert-danger">'+ errordata.q_3[0] + '</div>');
            }  else if (errordata.hasOwnProperty('q_4')) {
                validationAlert.html('<div class="alert alert-danger">'+ errordata.q_4[0] + '</div>');
            } else if (errordata.hasOwnProperty('q_5')) {
                validationAlert.html('<div class="alert alert-danger">'+ errordata.q_5[0] + '</div>');
            } else if (errordata.hasOwnProperty('q_6')) {
                validationAlert.html('<div class="alert alert-danger">'+ errordata.q_6[0] + '</div>');
            } else if (errordata.hasOwnProperty('q_7')) {
                validationAlert.html('<div class="alert alert-danger">'+ errordata.q_7[0] + '</div>');
            } else if (errordata.hasOwnProperty('query')) {
                validationAlert.html('<div class="alert alert-danger">'+ errordata.query[0] + '</div>');
            } else if (errordata.hasOwnProperty('exception')) {
                validationAlert.html('<div class="alert alert-danger">'+ errordata.exception[0] + '</div>');
            }
        },
        success: function(response) {
            validationAlert.html('<div class="alert alert-success">You have successfully submitted the survey.</div>');
            setTimeout(() => {
                location.href = HOST_URL +'/';
            }, 2000);
        }
    });
});
