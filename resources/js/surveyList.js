$(document).ready(function() {
    let surveyListHtml = '';

    $.ajax({
        url: HOST_URL +'/api/v1/surveys/1/answers',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        type: 'GET',
        dataType: 'json',
        error: function(response) {
            const errordata = JSON.parse(response.responseText).errors;

            if (errordata.hasOwnProperty('exception')) {
                validationAlert.html('<div class="alert alert-danger">'+ errordata.exception[0] + '</div>');
            }
        },
        success: function(response) {
            let i, j, k;
            for (i = 0; i < response.surveyAnswers.length; ++i) {
                surveyListHtml += `<div class="d-grid gap-3" id="survey-block">
<table class="table table-bordered">
<tr>
<td>Survey Name</td>
<td>${response.surveyAnswers[i].survey.name}</td>
</tr>`;

                for (j = 0; j < response.surveyAnswers[i].details.length; ++j) {
                    if (parseInt(response.surveyAnswers[i].details[j].question.question_type_id) === 1) {
                        surveyListHtml += `<tr>
<td>${response.surveyAnswers[i].details[j].question.text}</td>
<td>${response.surveyAnswers[i].details[j].text}</td>
</tr>`
                    } else if (parseInt(response.surveyAnswers[i].details[j].question.question_type_id) === 2) {
                        surveyListHtml += `<tr>
<td>${response.surveyAnswers[i].details[j].question.text}</td>
<td>${response.surveyAnswers[i].details[j].choice.text}</td>
</tr>`;
                    } else if (parseInt(response.surveyAnswers[i].details[j].question.question_type_id) === 3) {
                        const multipleChoices = response.surveyAnswers[i].details[j].text.split(',')

                        for (k = 0; k < multipleChoices.length; ++k) {
                            surveyListHtml += `<tr>
<td>${response.surveyAnswers[i].details[j].question.text}</td>
<td>${response.surveyAnswers[i].details[j].choice.text}</td>
</tr>`;
                        }
                    }
                }

                    surveyListHtml += `<tr>
<td>Respondent's Name</td>
<td>${response.surveyAnswers[i].respondent ? response.surveyAnswers[i].respondent.name : ''}</td>
</tr>
<tr>
<td>Respondent's Phone</td>
<td>${response.surveyAnswers[i].respondent ? response.surveyAnswers[i].respondent.phone : ''}</td>
</tr>
<tr>
<td>Respondent's Email</td>
<td>${response.surveyAnswers[i].respondent ? response.surveyAnswers[i].respondent.email : ''}</td>
</tr>
</table>
</div><hr />`;
            }
            $('#all_surveys').html(surveyListHtml);
        }
    });
});
