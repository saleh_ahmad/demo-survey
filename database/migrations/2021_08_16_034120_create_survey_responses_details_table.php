<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyResponsesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_responses_details', function (Blueprint $table) {
            $table->id();

            $table->integer('master_id')
                ->comment('Survey Response Master ID')
                ->default(0);
            $table->integer('question_id')
                ->comment('Survey Question ID')
                ->default(0);
            $table->text('text')
                ->comment('Survey Question\'s Answer')
                ->nullable();
            $table->integer('choice_id')
                ->comment('Survey Question\'s Choice ID')
                ->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_responses_details');
    }
}
