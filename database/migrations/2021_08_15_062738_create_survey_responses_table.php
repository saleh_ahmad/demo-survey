<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_responses', function (Blueprint $table) {
            $table->id();

            $table->integer('respondent_id')
                ->comment('Survey Respondent ID')
                ->default(0);
            $table->integer('survey_id')
                ->comment('Survey ID')
                ->default(0);
            $table->integer('question_id')
                ->comment('Survey Question ID')
                ->default(0);
            $table->text('text')
                ->comment('Survey Question\'s Answer')
                ->nullable();
            $table->integer('choice_id')
                ->comment('Survey Question\'s Choice ID')
                ->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_responses');
    }
}
