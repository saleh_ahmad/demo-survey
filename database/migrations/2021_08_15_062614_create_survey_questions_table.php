<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_questions', function (Blueprint $table) {
            $table->id();

            $table->string('text')
                ->comment('Question Text')
                ->nullable();
            $table->integer('question_type_id')
                ->comment('Survey Question Type')
                ->default(0);
            $table->integer('survey_id')
                ->comment('Survey ID')
                ->default(0);
            $table->integer('order_by')
                ->comment('Question Order')
                ->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_questions');
    }
}
