<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyQuestionChoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_question_choices', function (Blueprint $table) {
            $table->id();

            $table->string('text')
                ->comment('Survey Question Choice')
                ->nullable();
            $table->text('description')
                ->comment('Survey Description')
                ->nullable();
            $table->integer('question_id')
                ->comment('Survey Question ID')
                ->default(0);
            $table->integer('next_question_id')
                ->comment('Survey Next Question ID')
                ->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_question_choices');
    }
}
