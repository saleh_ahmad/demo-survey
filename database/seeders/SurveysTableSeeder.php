<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SurveysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('surveys')->insert([
            [
                'name'        => 'Test Survey',
                'description' => '',
                'updated_at'  => now(),
                'created_at'  => now()
            ]
        ]);
    }
}
