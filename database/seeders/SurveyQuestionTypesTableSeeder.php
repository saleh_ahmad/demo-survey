<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SurveyQuestionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('survey_question_types')->insert([
            [
                'name'        => 'Text',
                'description' => '',
                'updated_at'  => now(),
                'created_at'  => now()
            ],
            [
                'name'        => 'Single Choice',
                'description' => '',
                'updated_at'  => now(),
                'created_at'  => now()
            ],
            [
                'name'        => 'Multiple Choice',
                'description' => '',
                'updated_at'  => now(),
                'created_at'  => now()
            ]
        ]);
    }
}
