<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SurveyQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('survey_questions')->insert([
            [
                'text'             => 'Question 1',
                'question_type_id' => 2,
                'survey_id'        => 1,
                'order_by'         => 1,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
            [
                'text'             => 'Question 2',
                'question_type_id' => 3,
                'survey_id'        => 1,
                'order_by'         => 2,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
            [
                'text'             => 'Question 3',
                'question_type_id' => 1,
                'survey_id'        => 1,
                'order_by'         => 3,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
            [
                'text'             => 'Question 4',
                'question_type_id' => 1,
                'survey_id'        => 1,
                'order_by'         => 4,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
            [
                'text'             => 'Question 5',
                'question_type_id' => 2,
                'survey_id'        => 1,
                'order_by'         => 5,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
            [
                'text'             => 'Question 6',
                'question_type_id' => 1,
                'survey_id'        => 1,
                'order_by'         => 6,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
            [
                'text'             => 'Question 7',
                'question_type_id' => 2,
                'survey_id'        => 1,
                'order_by'         => 7,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
            [
                'text'             => 'Question 8',
                'question_type_id' => 1,
                'survey_id'        => 1,
                'order_by'         => 8,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
        ]);
    }
}
