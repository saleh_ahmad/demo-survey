<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SurveyQuestionChoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('survey_question_choices')->insert([
            [
                'text'             => 'Q1C1',
                'description'      => '',
                'question_id'      => 1,
                'next_question_id' => 3,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
            [
                'text'        => 'Q1C2',
                'description' => '',
                'question_id' => 1,
                'next_question_id' => 5,
                'updated_at'  => now(),
                'created_at'  => now()
            ],
            [
                'text'        => 'Q1C3',
                'description' => '',
                'question_id' => 1,
                'next_question_id' => 0,
                'updated_at'  => now(),
                'created_at'  => now()
            ],
            [
                'text'        => 'Q2C1',
                'description' => '',
                'question_id' => 2,
                'next_question_id' => 0,
                'updated_at'  => now(),
                'created_at'  => now()
            ],
            [
                'text'        => 'Q2C2',
                'description' => '',
                'question_id' => 2,
                'next_question_id' => 0,
                'updated_at'  => now(),
                'created_at'  => now()
            ],
            [
                'text'        => 'Q2C3',
                'description' => '',
                'question_id' => 2,
                'next_question_id' => 0,
                'updated_at'  => now(),
                'created_at'  => now()
            ],
            [
                'text'             => 'Q5C1',
                'description'      => '',
                'question_id'      => 5,
                'next_question_id' => 8,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
            [
                'text'             => 'Q5C2',
                'description'      => '',
                'question_id'      => 5,
                'next_question_id' => 0,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
            [
                'text'             => 'Q5C3',
                'description'      => '',
                'question_id'      => 5,
                'next_question_id' => 7,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
            [
                'text'             => 'Q7C1',
                'description'      => '',
                'question_id'      => 7,
                'next_question_id' => 0,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
            [
                'text'             => 'Q7C2',
                'description'      => '',
                'question_id'      => 7,
                'next_question_id' => 0,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
            [
                'text'             => 'Q7C3',
                'description'      => '',
                'question_id'      => 7,
                'next_question_id' => 0,
                'updated_at'       => now(),
                'created_at'       => now()
            ],
        ]);
    }
}
